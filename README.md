# Music Converter

## The aim

This program takes your music folder and makes a copy of it but with all the files converted to a different format (or just bitrate).
Really helps out when you store your music in a format some other device, like your phone (or mp3 player if you live in the past like me), doesn't understand.

The converting is done by calling [ffmpeg](https://ffmpeg.org/).
Music Converter is basically an (awesome) automation that spares you from having to call it for every single individual file by hand, because ffmpeg doesn't handle folders as inputs.
Music Converter also grew to do so intelligently to minimise conversions across subsequent runs on the same folders.

## How to use it

#### A word for the newbies out there (we all were one sometime)

Music Converter is a CLI program, which means you need to open a terminal to use it properly.
If you don't know what a terminal is, then maybe it's not for you, but if you really wanna know, bug me by [making an issue](https://gitlab.com/matsaki95/music-converter/issues).
It is also written for Linux based distributions.
It may work with other operating systems, but it would require effort that in the case of Windows includes editing the code. MacOS and BSD would work fine without changes, probably, but I haven't ever touched the stuff to know for sure, so don't quote me on that.

Otherwise, if you know how to open the terminal, I'll assume you also know how to call commands and pass basic arguments.
If you don't, I'll prompt you to [this simple guide](https://www.pcsteps.com/5010-basic-linux-commands-terminal/) to get started in becoming a _CLI wizard_.


#### Installation

##### Requirements

At first we need to install the tools necessary to build the program.
Specifically, we need the `g++` compiler, `make`, the `boost filesystem library`, the `boost system library`, the `crypto++ library` and `git`.

If you're using Ubuntu or a Ubuntu-based distribution the requirements can be installed by running: `sudo apt install build-essential libboost-system-dev libboost-filesystem-dev libcrypto++-dev git`.

##### Building and installing from source

After installing the requirements, we can download the program's source code by running `git clone https://gitlab.com/matsaki95/music-converter`.
Then go into the newly created folder by running `cd music-converter`, run `make install` and follow the further instructions it will print out.

#### Usage guide

##### Going default

The simplest way to use Music Converter is without setting any options, which means just calling `music-converter`.
The program will then assume the _source_ (meaning the folder with the files you want to convert) is the Music folder under your home directory (`~/Music`).
It will also assume the _destination_ is `~/Music-Converted`, meaning it will create a folder next to the _source_ called _Music-Converted_ and the converted files will be in there.
Music Converter also assumes that all files read are readable by ffmpeg and will pass all of them to ffmpeg for them to be converted to `.ogg` files with a 320k bitrate.

##### Getting help

Running `music-converter -h` is a great reference/cheat sheet to all the options written in the sections below.
It makes the program simply display a bit of (precious) info and exit without doing anything else.

##### Changing the source and destination

You can change both the source and the destination.
- To change the source you go `music-converter Some/Other/Source/Folder/`.
- Changing the destination is as simple as supplying one more folder in the arguments like `music-converter Some/Other/Source/Folder/ Some/Different/Destination/Folder`.

##### Excluding files/folders

You may have some folders or files you don't want to include in the conversion.
For that, there is the `-e`/`--exclude` switch, which you can use an arbitrary amount of times (that means as much as you want).
- `music-converter --exclude Instrumental` will cause the program to ignore any files/folders called _Instrumental_.
- `music-converter -e Instrumental -e "Carly Rae Jepsen - Call Me Maybe.opus"` will make it so that in addition to ignoring files/folders called _Instrumental_ like above, any files/folders called _Carly Rae Jepsen - Call Me Maybe.opus_ will get ignored too.

##### Adjusting the bitrate

Perhaps you feel 320k bitrate is too much quality for you.
Maybe the files don't fit in your phone because of that or your mp3 player is a cheap knockoff with a shoddy DAC so it doesn't matter anyway.
Whatever the reason, the `b`/`--bitrate` switch allows you to set a bitrate of your choice.
- `music-converter --bitrate 256k` will have your music converted with a modest bitrate so as to save some space.
- `music-converter -b 128k ~/Music /path/to/phone/Music` maybe will allow you to cram your excessive _10 GB library_ into your phone... though the quality would be pretty questionable.

##### Converting only select formats

So you have files in various formats and you want to convert a specific one?
Or there are files in your _source_ that are not music files?
Fret not!
For yee shall be provided of a solution for thee problems.
The solution that yee may find right in there here in this here section, where yee can see how one may use, what is provided, in this here section.
This section that you're reading.
The part which tells the following, the solution to thy problem.
The `-i`/`--input-extension` switch.
It tells Music Converter to ignore any file extension that you didn't specify.
- `music-converter --input-extension flac` would make it so that only files with the _flac_ extension are used as input, ignoring any other kind of file.
- `music-converter -i opus -i ogg -i flac -i pcm -i wav -i mp3 -i aac` is a cocktail that would most likely cover you if you just have no idea what you want out of your _source_.

##### Converting to a select format

Vorbis files are not your thing?
Or maybe not your mp3 player's thing?
Fret not!
For -ahem, sorry.
If you want a different output format, you can use the `-o`/`--output` switch.
There is a twist to this one though: it's most likely you'll also have to do a tiny bit of digging because you'll also have to pass the `-l`/`--ffmpeg-library` switch.
Run `ffmpeg -codecs` for a big list of codecs and look for the encoder matching your extension of choice.
- `music-converter -o mp3 -l libmp3lame` will convert to mp3 instead the default ogg.
- `music-converter -o mp2 -l libtwolame` means we get it; you just want to watch the world _**burn**_...

##### Verbosity

While the default option is perfect, since it lets you see the program's progress across the files it converts and lets any error ffmpeg may have show up, you can still change the amount of text output by using the `-v`/`--verbose` and `-q`/`--quiet` switches.
`-v` increases verbosity by 1 and `-q` decreases it by the same amount.
The default verbosity level is 1 and while it can go higher than 2, there's no difference beyond it.
- `music-converter -q "80s Hits"` will make the program convert the folder _80s Hits_ but instead of out outputting its progress like it would do normally, it will only output ffmpeg errors (if there are any, of course).
- `music-converter -v` will have the program let ffmpeg to output its own progress over the conversion of each <del>and every</del> file (you still get the usual program's progress too).

##### Simulation

If you, for some reason (perhaps testing something), don't want the program to make any actual changes to any files, just pass the `-s`/`--simulation` switch to refrain it from doing so.
- `music-converter -e Draft -s Music-Testing` will have Music Converter going through its usual procedure (excluding files/folders called _Draft_ because of the `-e` switch and using _Music-Testing_ as the source) but the program will never actually make any changes to the filesystem, leaving all your files safe and sound.
