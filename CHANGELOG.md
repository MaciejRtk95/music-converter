### Music Converter v2.1: The more, the merrier, because the faster

What if I told you that you can finish your task twice as fast if there's two of you? Well... Theoretically... To a point that's not exactly twice...

- First introduction of using multiple threads. Hashing is now distributed among all the cores of your processor instead of being limited to one. That means possibly faster hashing. Note that you're still limited by other factors like whether the files are on your hard drive or cached in your RAM by your operating system. But even if completely limited, it's still better because while one thread loads the file, the others can do actual work, so the bandwidth is utilised the whole time instead of being idle once a file is loaded. There will be an option to disable this in the future.
- The solution finding algorithm had a minor bug where it wouldn't delete some files in specific circumstances. It's fixed now.
- Cleaned up the code a bit.



### Music Converter v2: Learning from the past

No more do you have to wait every time for **all** of your files to be converted.
This version includes a cache system so that only what is needed is converted.

- There's now a caching system that hashes the source files and saves the hashes at the end of the run for use next time the program runs. When ran again, the old hashes are loaded and are compared with the new by the new and shiny algorithm which figures out what has to be deleted and what has to be converted. This is a massive and beautiful improvement over the previous version of the program which just wastefully deleted the destination directory at startup.
- You now have control over the basic conversion parameters, those being the bitrate (`-b`/`--bitrate`) and the output format (`-o`/`--output-extension` and `-l`/`--ffmpeg-library`).
- If there's a need to convert only specific types of files, you can now use the `-i`/`--input-extension` switch to filter files by extension.
As with the `-e`/`--exclude` switch implemented in the previous version, you can pass as many of these as you like.



### Music Converter v1: Improvise, Adapt, Overcome

The first step of evolution.
Form a simple hardcoded script, Music Converter got promoted to a proper automation program with its new abilities being:

- The file/folder structure isn't as limited as before, the program now properly goes recursively down the folder tree.
- You can select yourself the source and destination.
If you only supply the source, the destination is the same as the source with `-Converted` slapped to the back of the source's folder name.
If you don't supply anything, the source automatically becomes your home's `Music` folder.
- Exclusion now has to be specified with the `-e`/`--exclusion` switch.
Then, when reading the folder structure, the program checks if any argument matches the file/folder name and ignores the folder/file if it does.
- Instead of letting ffmpeg output a blob of text for each file it converts, the program now outputs its own progress report in a pretty and compact way.
Changeable using the `-v`/`--verbose` and `-q`/`--quiet` switches.
- Added a simulation mode where the program runs normally but without ever touching the filesystem.
You can enable it with the `-s`/`--simulation` switch.



### Music Converter v0: All your automation are belong to us

Initial, hardcoded-values _scripty_ version

- Reads specific folder structure under `/home/matsaki95/Music` and converts the Opus files it finds to Ogg, placing them under `/home/matsaki95/Music-Converted`
- Ignores folders called _Draft_