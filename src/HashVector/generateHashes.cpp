#include "HashVector.h"
#include <crypto++/sha.h>
#include <crypto++/hex.h>

#define CHUNK_SIZE 1000000

std::string HashVector::generateHash(const std::string& inputPath)
{
    std::ifstream input(inputPath, std::ifstream::binary);

    input.seekg(0, std::ifstream::end);
    auto inputLength = (unsigned long long) input.tellg();
    input.seekg(0, std::ifstream::beg);

    CryptoPP::SHA256 hash;
    char buffer[CHUNK_SIZE];
    unsigned long long remainingLength = inputLength;
    while (remainingLength>CHUNK_SIZE) {
        input.readsome(buffer, CHUNK_SIZE);
        hash.Update((byte*) buffer, CHUNK_SIZE);
        remainingLength -= CHUNK_SIZE;
    }
    input.readsome(buffer, (std::streamsize) remainingLength);
    input.close();
    hash.Update((byte*) buffer, remainingLength);

    byte digest[CryptoPP::SHA256::DIGESTSIZE];
    hash.Final(digest);

    std::string output;
    CryptoPP::HexEncoder encoder;
    encoder.Attach(new CryptoPP::StringSink(output));
    encoder.Put(digest, sizeof(digest));
    return output;
}

void HashVector::generateHashes(std::queue<std::string>& fileQueue, std::atomic<unsigned>& currentFile, const unsigned totalFiles)
{
    while (true) {
        fileQueueMutex.lock();
        if (fileQueue.empty()) {
            fileQueueMutex.unlock();
            break;
        }
        std::string file = fileQueue.front();
        fileQueue.pop();
        fileQueueMutex.unlock();

        ++currentFile;
        if (programOptions.verbosity>0)
            std::cout << "\r\e[J[" << currentFile << '/' << totalFiles << "] Generating new hash for " << boost::filesystem::basename(file) << "..." << std::flush;
        std::string hash = generateHash(file);

        hashesMutex.lock();
        add(file, hash);
        hashesMutex.unlock();
    }
}
