#include <iostream>
#include "ProgramOptions.h"

void printUsage()
{
    std::cout << "Music Converter takes your music folder and makes a copy of it but with all the files converted to a different format or bitrate. Really helps out when you store your music in a format some other device, like your phone (or mp3 player if you live in the past like me), doesn't understand.\n\n"
              << "Usage: " << programOptions.programCall << " [options] [source_folder] [destination_folder]\n\n"
              << "Options:\n"
              << "    -h, --help                Displays this screen.\n"
              << "    -e, --exclude             Excludes any directory or file with the exact same name. You can pass many of these.\n"
              << "    -b, --bitrate             Specifies what bitrate to use instead of the default 320k.\n"
              << "    -i, --input-extension     Applies a filter based on the file extension to the files used as input. You can pass many of these to widen your filter.\n"
            << "    -o, --output-extension    Sets what extension the outputted files will have instead of the default ogg.\n"
            << "    -l, --ffmpeg-library      If you change the default extension, you'll probably have to change the library ffmpeg will use appropriately. Run `ffmpeg -codecs` for a big list of available codecs (you'll be looking for the encoder matching your extension).\n"
            << "    -v, --verbose             Increases verbosity by 1. Verbosity defaults at 1, with which the program outputs its progress report. A verbosity of 0 outputs nothing. A verbosity of 2 lets ffmpeg output too.\n"
            << "    -q, --quiet               Decreases verbosity by 1. Look at --verbose for more info.\n"
            << "    -s, --simulation          Runs the program without modifying the filesystem.\n\n"
            << "Music Converter v2.1\n"
            << "Report bugs, ideas and problems at gitlab.com/matsaki95/music-converter/issues" << std::endl;
}
