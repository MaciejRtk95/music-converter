#include <iostream>
#include <algorithm>
#include "ProgramOptions.h"
#include "../RootNodes.h"
#include "printUsage.h"

void ProgramOptions::parseProgramOptions(int argc, char* argv[])
{
    programCall = argv[0];

    int longIndex = 0;
    int opt = getopt_long(argc, argv, shortOptions, longOptions, &longIndex);
    while (opt!=-1) {
        switch (opt) {
        case '?':
        case 'h':
            printUsage();
            exit(0);
        case 'e':
            filenamesToExclude.emplace_back(optarg);
            break;
        case 'b':
            bitrate = optarg;
            break;
        case 'i':
            inputExtensions.emplace_back(optarg);
            break;
        case 'o':
            outputExtension = optarg;
            break;
        case 'l':
            ffmpegLibrary = optarg;
            break;
        case 'v':
            ++verbosity;
            break;
        case 'q':
            if (verbosity>0)
                --verbosity;
            break;
        case 's':
            simulationMode = true;
            break;
        default:
            std::cout << "\e[31mIf you're seeing this, something went \e[1mterribly wrong\e[0;31m in the \e[1mprogram options\e[0;31m department.\e[0m" << std::endl;
            exit(95);
        }
        opt = getopt_long(argc, argv, shortOptions, longOptions, &longIndex);
    }

    //So we can binary search later
    std::sort(filenamesToExclude.begin(), filenamesToExclude.end());
    std::sort(inputExtensions.begin(), inputExtensions.end());

    for (auto& extension : inputExtensions) {
        if (extension[0]!='.')
            extension.insert(0, ".");
    }
    if (outputExtension[0]!='.')//It surely isn't empty, because then getopt_long would complain
        outputExtension.insert(0, ".");

    switch (argc-optind) {
    case 2:
        destination = argv[optind+1];//Falls through to case 1
    case 1:
        source = argv[optind];
        break;
    case 0:
        break;
    default:
        printUsage();
        std::cout << "\e[31mError: Supplied source (" << argv[optind] << "), supplied destination (" << argv[optind+1] << ")... \e[1mWhat's is the third supposed to be (" << argv[optind+2] << ")?\e[0m" << std::endl;
        exit(1);
    }

    if (source.empty()) {
        source = getenv("HOME");
        source += "/Music";
    }
    if (destination.empty()) {
        destination = source;
        while (destination.back()=='/') {
            destination.pop_back();
        }
        destination += "-Converted/";
    }
    rootNodes.source.path = boost::filesystem::absolute(source);
    rootNodes.source.isAFolder = true;
    rootNodes.destination.path = boost::filesystem::absolute(destination);
    rootNodes.destination.isAFolder = true;

    if (rootNodes.source.path==rootNodes.destination.path) {
        std::cout << "\e[31mError: The destination directory can't be the same as the source directory.\e[0m" << std::endl;
        exit(2);
    }
    if (!boost::filesystem::exists(rootNodes.source.path)) {
        std::cout << "\e[31mError: The source directory (" << rootNodes.destination.path << ") doesn't exist.\e[0m" << std::endl;
        exit(3);
    }
    if (!boost::filesystem::is_directory(rootNodes.source.path)) {
        std::cout << "\e[31mError: The source directory (" << rootNodes.destination.path << ") is not a directory.\e[0m" << std::endl;
        exit(4);
    }
    if (boost::filesystem::exists(rootNodes.destination.path) && !boost::filesystem::is_directory(rootNodes.destination.path)) {
        std::cout << "\e[31mError: The destination directory (" << rootNodes.destination.path << ") is not a directory.\e[0m" << std::endl;
        exit(5);
    }
}
